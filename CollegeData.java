public class CollegeData {
  // data fields
  private String collegeName;
  /* Each of these represent average acceptance data for colleges */
  private double acceptanceRate, gpaScore25, gpaScore75, gpaAverage;
  private int satScore25, satScore75, satScoreAverage;
  /* Each of these represent the importance of other factors for each college, rated 1 - 4 */
  private double rigor, essay, extracurricular, workExperience, volunteer, firstGen, residency, ethnicity;

  public CollegeData(String collegeName, double acceptanceRate, double gpaScore25, double gpaScore75, double gpaAverage, int satScore25, int satScore75, int satScoreAverage, double rigor, double essay, double extracurricular, double workExperience, double volunteer, double firstGen, double residency, double ethnicity) {
    this.collegeName = collegeName;
    this.acceptanceRate = acceptanceRate;
    this.gpaScore25 = gpaScore25;
    this.gpaScore75 = gpaScore75;
    this.gpaAverage = gpaAverage;
    this.satScore25 = satScore25;
    this.satScore75 = satScore75;
    this.satScoreAverage = satScoreAverage;
    this.rigor = rigor;
    this.essay = essay;
    this.extracurricular = extracurricular;
    this.workExperience = workExperience;
    this.volunteer = volunteer;
    this.firstGen = firstGen;
    this.residency = residency;
    this.ethnicity = ethnicity;
  }

  // Getter methods
  public String getCollegeName() {
    return collegeName;
  }

  public double getAcceptanceRate() {
    return acceptanceRate;
  }

  public double getGpaScore25() {
    return gpaScore25;
  }

  public double getGpaScore75() {
    return gpaScore75;
  }

  public double getGpaAverage() {
    return gpaAverage;
  }

  public double getSatScore25() {
    return satScore25;
  }

  public double getSatScore75() {
    return satScore75;
  }

  public double getSatScoreAverage() {
    return satScoreAverage;
  }

  public double getRigor() {
    return rigor;
  }

  public double getEssay() {
    return essay;
  }

  public double getExtracurricular() {
    return extracurricular;
  }

  public double getWorkExperience() {
    return workExperience;
  }

  public double getVolunteer() {
    return volunteer;
  }

  public double getFirstGen() {
    return firstGen;
  }

  public double getResidency() {
    return residency;
  }

  public double getEthnicity() {
    return ethnicity;
  }
}
