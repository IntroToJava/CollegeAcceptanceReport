# College Acceptance Report
A Java CMD line application to calculate a user's percent chance of getting accepted into a list of colleges.

- Built for CS 1A at Foothill College
- June 2016
- Team Members: Taaha Chaudhry, Ivan Vasilko, Yash Wani

## Process
As a team, we used Git and Gitlab to follow current development processes and method of having code collaboration and management. We began by setting up Gitlab issues (tasks), and understand the structure, flow, and problems we needed to solve for our application.
The most complex part of our application was figuring out the logic and math to be able to return a "relatively accurate" college acceptance percentage score.
Following common development practices, we decided to have a workflow using merge requests, solving merge conflicts, and having code reviews as a team.
We also made sure to modularize our code for readability and reusability.

## Data Storage
Based on data pulled from https://collegedata.com, we implemented a college data class to store data for each university.
This CollegeData class contained data fields, constructor, and getter methods. Each object constructor set the data fields such as GPA, SAT scores, and importance of other factors for universities i.e. extracurriculars, volunteer work etc. Each college is an instance of this CollegeData class and we stored the list of colleges in an array and used them by looping over to compare against user inputted data.

## Logic Behind Our Program
We rely on the assumption that students are admitted into colleges along a bell curve (standard normal distribution).
Using data pulled from https://collegedata.com, we calculate the standard deviation and subsequently the z-score (a measure of standard deviations above or below the population mean) based on SAT and GPA scores for each university compared against the user's inputs. This total GPA + SAT score then is converted into a base percentage.
We then evaluate "other criteria" such as rigor, essay quality, extracurricular quality, volunteer work, paid work, first generation, residency, ethnicity on a scale from 1-10. We compare each of these scores to a mean score we derive from the acceptance rates of the individual schools.
We then weight each criteria based on its importance to an individual university. This data comes from https://collegedata.com.
The "other criteria" score is then added or subtracted from the SAT and GPA score to deliver a final percent chance of acceptance for the list of universities.
The lowest chance is set at 5% and the highest chance is set at 95%.  

## To compile and run the program
**From the root of this repository**
1. Download a zip file or clone of this repo
2. `javac CollegeAcceptanceReport.java` to compile
3. `java CollegeAcceptanceReport` to run
