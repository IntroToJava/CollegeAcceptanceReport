/*
* College Acceptance Report
* CS 1A Foothill College
* June 2016
*
* Team Members: Taaha Chaudhry, Ivan Vasilko, Yash Wani
* Presentation can be found in the README doc on Gitlab: https://gitlab.com/IntroToJava/CollegeAcceptanceReport
*/

import java.util.Scanner;

public class CollegeAcceptanceReport {
	public static void main(String[] args) {
    // Formatting program introduction
    System.out.println(String.format("\n%35s", "").replace(' ', '*'));
    System.out.printf("%30s\n", "College Acceptance Report");
    System.out.println(String.format("%35s", "").replace(' ', '*'));

    String intro = "\nA Java CMD line application to calculate a high school student's percent chance of getting accepted into a list of colleges. Disclaimer: Regardless of our perfect logic, math, and algorithm, this acceptance report may not be accurate. Please do not rely your chances of acceptance based on this report.";
    System.out.println(intro);
    System.out.println(String.format("%" + intro.length() / 2 + "s\n", "").replace(' ', '-'));

    // Asking for user inputs
		Scanner input = new Scanner(System.in);

    System.out.print("What is your name: ");
		String name = input.nextLine();

		System.out.print("Enter your GPA score (2.0-5.0): ");
		double gpaInput = input.nextDouble();
    while (gpaInput < 2.0 || gpaInput > 5.0 ) {
      System.out.print("Your GPA score input is invalid.\n" + "Enter a valid GPA score (2.0-5.0): ");
      gpaInput = input.nextDouble();
    }

		System.out.print("Enter your SAT score (600-2400): ");
		int satInput = input.nextInt();
    while (satInput < 600 || satInput > 2400 ) {
      System.out.print("Your SAT score input is invalid.\n" + "Enter a valid SAT score (600-2400): ");
      satInput = input.nextInt();
    }

		System.out.print("Enter the number of AP classes or college classes you have taken (academic rigor): ");
		int rigorInput = input.nextInt();
    while (rigorInput < 0 ) {
      System.out.print("Your academic rigor input is invalid.\n" + "Enter a minimum of zero: ");
      rigorInput = input.nextInt();
    }

		System.out.print("Rate your essay quality from 1-10: ");
		int essayInput = input.nextInt();
    while (essayInput < 1 || essayInput > 10 ) {
      System.out.print("Your essay quality input is invalid.\n" + "Enter a valid essay quality score (1-10): ");
      essayInput = input.nextInt();
    }

		System.out.print("Rate your extracurricular activites from 0-10: ");
		int ecInput = input.nextInt();
    while (ecInput < 0 || ecInput > 10 ) {
      System.out.print("Your extracurricular activites input is invalid.\n" + "Enter a valid extracurricular activites score (0-10): ");
      ecInput = input.nextInt();
    }

		System.out.print("Enter your total number of hours volunteered: ");
		int volunteerInput = input.nextInt();
    while (volunteerInput < 0 ) {
      System.out.print("Your volunteered hours input is invalid.\n" + "Enter a minimum of zero: ");
      volunteerInput = input.nextInt();
    }

    System.out.print("Enter the number of paid work jobs: ");
    int workInput = input.nextInt();
    while (workInput < 0 ) {
      System.out.print("Your paid work hours input is invalid.\n" + "Enter a minimum of zero: ");
      workInput = input.nextInt();
    }

		System.out.print("Are you a first generation college student (Y/N): ");
		char firstGenInput = Character.toUpperCase(input.next().charAt(0));
    while (firstGenInput != 'Y' && firstGenInput != 'N') {
      System.out.print("Invalid answer. Please answer Yes (Yes/Y/y) or No (No/N/n): ");
      firstGenInput = Character.toUpperCase(input.next().charAt(0));
    }

		System.out.print("Are you an in-state resident (Y/N): ");
		char residencyInput = Character.toUpperCase(input.next().charAt(0));
    while (residencyInput != 'Y' && residencyInput != 'N') {
      System.out.print("Invalid answer. Please answer Yes (Yes/Y/y) or No (No/N/n): ");
      residencyInput = Character.toUpperCase(input.next().charAt(0));
    }

		System.out.print("What is your ethnicity category (1- Asian, 2- Caucasian, 3- Latino/Hispanic/African American, 4- American Indian/Alaska Native, 5- Prefer not to respond): ");
		int ethnicityInput = input.nextInt();
    while (ethnicityInput < 1 || ethnicityInput > 5 ) {
      System.out.print("Your ethnicity category input is invalid.\n" + "Enter a valid ethnicity category (1-5): ");
      ethnicityInput = input.nextInt();
    }

    System.out.println();

    System.out.printf("%" + (int)(50 + name.length() + 28)/ 2 + "s\n", name + "'s College Acceptance Report");
    System.out.println(String.format("%50s", "").replace(' ', '-'));

    /*
    * Instantiate each college with data
    * Return an array of colleges
    *
    */
    CollegeData[] collegesList = instantiateColleges();

    // Loop over list of college names and calculate/display the college acceptance report
    for (int i = 0; i < collegesList.length; i++) {

      // Calculate base GPA and SAT scores
      double totalScore = calculateGPAandSATScores(collegesList[i], gpaInput, satInput);

      // Calculate college acceptance rates for all other factors
      double finalScore = calculateCollegeAcceptanceRates(collegesList[i], totalScore, rigorInput, essayInput, ecInput, volunteerInput, workInput, firstGenInput, residencyInput, ethnicityInput);

      // Display calculated college acceptance rates for the list of colleges
      displayCalculatedCollegeReport(collegesList[i], finalScore);
    }

    System.out.println();
	}

  public static double calculateGPAandSATScores(CollegeData college, double gpaInput, double satInput) {
    // Initializing GPA and SAT scores for each college
    double gpaScore25 = college.getGpaScore25();
    double gpaScore75 = college.getSatScore75();
    double gpaScoreAverage = college.getGpaAverage();
    double satScore25 = college.getSatScore25();
    double satScore75 = college.getSatScore75();
    double satScoreAverage = college.getSatScoreAverage();

    // Equation: z score = (x - mean) / std dev

    // zScore 25th percentile
    double zScore25 = -0.675;

    // Calculating the GPA standard deviation
    double gpaStd = (gpaScore25 - gpaScoreAverage) / zScore25;

    // Calculating the SAT score standard deviation
    double satStd = (satScore25 - satScoreAverage ) / zScore25;

    // Calculating the student z score
    double gpaZScore = (gpaInput - gpaScoreAverage) / gpaStd;
    double satZScore = (satInput - satScoreAverage) / satStd;

    // Calculating the weighted Z score, assuming GPA is 2x weight of SAT score
    gpaZScore = gpaZScore;
    satZScore = satZScore / 2;

    // Adding up z scores
    double totalScore = (gpaZScore + satZScore) * 100;

    return totalScore;
  }

  public static double calculateCollegeAcceptanceRates(CollegeData college, double totalScore, int rigorInput, int essayInput, int ecInput, int volunteerInput, int workInput, char firstGenInput, char residencyInput, int ethnicityInput) {
    int minTotalScore = -1224;
    int maxTotalScore = 494;

    int totalGPAandSATScore = (int)Math.round(totalScore); // rounding double to int
    // if totalScore is in between 25th(-1224) and 75th(494) it will be converted into percent accordingly
    double finalScore = 0;

    if (minTotalScore <= totalGPAandSATScore && totalGPAandSATScore <= maxTotalScore) {
      // converting into percent
      double totalScoreToPercent = (((200 + totalScore) / 200) * 100) / 2;
      int totalScoreToInt = (int) Math.round(totalScoreToPercent); //rounding double to int

      finalScore = totalScoreToInt +
                   rigorCalc(rigorInput, college.getAcceptanceRate(), college.getRigor()) +
                   essayCalc(essayInput, college.getAcceptanceRate(), college.getEssay()) +
                   ecCalc(ecInput, college.getAcceptanceRate(), college.getExtracurricular()) +
                   volunteerCalc(volunteerInput, college.getAcceptanceRate(), college.getVolunteer()) +
                   workCalc(workInput, college.getAcceptanceRate(), college.getWorkExperience()) +
                   firstGen(firstGenInput, college.getAcceptanceRate(), college.getFirstGen()) +
                   residencyCalc(residencyInput, college.getAcceptanceRate(), college.getResidency()) +
                   ethnicityCalc(ethnicityInput, college.getAcceptanceRate(), college.getEthnicity());

    }

    return finalScore;
  }

  public static void displayCalculatedCollegeReport(CollegeData college, double finalScore) {
    if (finalScore > 95) {
      System.out.printf("%-30s%20s\n", college.getCollegeName(), ">95%");
    }
    else if (finalScore < 5) {
      System.out.printf("%-30s%20s\n", college.getCollegeName(), "<5%");
    }
    else {
      System.out.printf("%-30s%20s\n", college.getCollegeName(), (int)Math.round(finalScore)+"%");
    }
  }

	public static double rigorCalc(int rigorInput, double collegeAcceptanceRate, double collegeImportanceRate) {
		double mean = (1 - collegeAcceptanceRate) * 10;
    // raw refers to the amount percentage added or subtracted when comparing the inputted score to the mean
    // scaledRigor gives a score between 0-10
    double scaledRigorInput = rigorInput / 2;
		double raw = (double)((scaledRigorInput - mean) / mean * 10) * collegeImportanceRate;
		return raw;
	}

	public static double essayCalc(int essayInput, double collegeAcceptanceRate, double collegeImportanceRate) {
		double mean = (1 - collegeAcceptanceRate) * 10;
		double raw = (double)((essayInput - mean) / mean * 10) * collegeImportanceRate;
		return raw;
	}

	public static double ecCalc(int ecInput, double collegeAcceptanceRate, double collegeImportanceRate) {
		double mean = (1 - collegeAcceptanceRate) * 10;
		double raw = (double)((ecInput - mean) / mean * 10) * collegeImportanceRate;
		return raw;
	}

	public static double volunteerCalc(int volunteerInput, double collegeAcceptanceRate, double collegeImportanceRate) {
		double mean = (1 - collegeAcceptanceRate) * 10;

    if (volunteerInput > 400) {
      volunteerInput = 400;
    }
    double scaledVolunteerInput = volunteerInput / 40;

		double raw = (double)((scaledVolunteerInput - mean) / mean * 10) * collegeImportanceRate;
		return raw;
	}

  public static double workCalc(int workInput, double collegeAcceptanceRate, double collegeImportanceRate) {
		double mean = (1 - collegeAcceptanceRate) * 10;
    double scaledWorkInput = workInput * 2;
		double raw = (double)((scaledWorkInput - mean) / mean * 10) * collegeImportanceRate;
		return raw;
	}

	public static double firstGen(int firstGenInput, double collegeAcceptanceRate, double collegeImportanceRate) {
		double mean = (1 - collegeAcceptanceRate) * 10;

    int firstGenAmount;

    if (firstGenInput == 'Y') {
      firstGenAmount = 10;
    } else {
      firstGenAmount = 0;
    }

		double raw = (double)((firstGenAmount - mean) / mean * 10) * collegeImportanceRate;
		return raw;
	}

	public static double residencyCalc(int residencyInput, double collegeAcceptanceRate, double collegeImportanceRate) {
		double mean = (1 - collegeAcceptanceRate) * 10;

    int residencyAmount;
    if (residencyInput == 'Y') {
      residencyAmount = 10;
    } else {
      residencyAmount = 0;
    }

		double raw = (double)((residencyAmount - mean) / mean * 10) * collegeImportanceRate;
		return raw;
	}

	public static double ethnicityCalc(int ethnicityInput, double collegeAcceptanceRate, double collegeImportanceRate) {
		double mean = (1 - collegeAcceptanceRate) * 10;

    int ethinityAmount;
    if (ethnicityInput == 3 || ethnicityInput == 4) {
      ethinityAmount = 10;
    } else {
      ethinityAmount = 0;
    }

		double raw = (double)((ethinityAmount - mean) / mean * 10) * collegeImportanceRate;
		return raw;
	}

  public static CollegeData[] instantiateColleges() {
    // Initialize list of CollegeData objects
    CollegeData[] collegesList = new CollegeData[12];

    /*
    * Factors rating of importance pulled from http://collegedata.com
    * acceptanceRate, gpaScore25, gpaScore75, gpaAverage, satScore25, satScore75, satScoreAverage, rigor, essay, extracurricular, workExperience, volunteer, firstGen, residency, ethnicity
    *
    */
    collegesList[0]  = new CollegeData("UC Berkeley",          0.16, 4.04, 4.19, 4.34, 1840, 2230, 2035, 0.99, 0.99, 0.66, 0.66, 0.66, 0.33, 0.33, 0);
    collegesList[1]  = new CollegeData("UCLA",                 0.19, 4.16, 4.31, 4.46, 1760, 2190, 1975, 0.99, 0.99, 0.66, 0.66, 0.66, 0.33, 0, 0);
    collegesList[2]  = new CollegeData("UC Irvine",            0.37, 3.79, 3.94, 4.09, 1550, 1930, 1740, 0.99, 0.99, 0.99, 0.99, 0.99, 0.33, 0.33, 0);
    collegesList[3]  = new CollegeData("UC San Diego",         0.38, 3.85, 4.00, 4.15, 1750, 2073, 1912, 0.99, 0.99, 0.66, 0.66, 0.66, 0.66, 0.66, 0);
    collegesList[4]  = new CollegeData("UC Davis",             0.41, 3.85, 4.00, 4.15, 1620, 2010, 1815, 0.99, 0.99, 0.66, 0.33, 0.66, 0.33, 0.33, 0);
    collegesList[5]  = new CollegeData("UC Santa Barbara",     0.36, 3.83, 3.98, 4.13, 1690, 2070, 1880, 0.99, 0.99, 0.33, 0.33, 0.33, 0.33, 0.33, 0);
    collegesList[6]  = new CollegeData("UC Riverside",         0.58, 3.53, 3.68, 3.83, 1510, 1860, 1685, 0.66, 0.66, 0.33, 0.33, 0.33, 0.33, 0.33, 0);
    collegesList[7]  = new CollegeData("SJSU",                 0.60, 3.23, 3.38, 3.53, 1340, 1760, 1550, 0.99, 0, 0, 0, 0, 0, 0.33, 0);
    collegesList[8]  = new CollegeData("Cal Poly Pomona",      0.52, 3.27, 3.42, 3.57, 1370, 1760, 1565, 0.99, 0, 0, 0, 0, 0, 0.33, 0);
    collegesList[9]  = new CollegeData("Cal Poly SLO",         0.31, 3.73, 3.88, 4.03, 1680, 1960, 1820, 0.99, 0, 0.33, 0.33, 0.33, 0.33, 0, 0);
    collegesList[10] = new CollegeData("Cal State Long Beach", 0.36, 3.37, 3.52, 3.67, 1526, 1750, 1638, 0.99, 0, 0, 0, 0, 0, 0, 0);
    collegesList[11] = new CollegeData("USC",                  0.18, 3.58, 3.73, 3.88, 1920, 2220, 2070, 0.99, 0.99, 0.66, 0.33, 0.33, 0.33, 0.33, 0.33);

    return collegesList;
  }

}
